﻿using Newtonsoft.Json;
using SecurityTask.CryptoAlgorithms;
using System;
using System.IO;
using System.Security.Cryptography;

namespace SecurityTask
{
    public class SymmetricCrypto : ICryptoAlgorithm
    {

        // encrypt file content, write cipherText to new file
        public void Encrypt(string dataFilePath, string parametersFilePath, string mode)
        {
            try
            {
                // read data to encrypt
                string plainText = File.ReadAllText(dataFilePath);

                // read encryption parameters from file
                SymCryptParameters parameters = ReadParametersFromFile(parametersFilePath);
                CipherMode cipherMode = StringToCipherMode(mode);

                byte[] encrypted = Encrypt(plainText, parameters, cipherMode);

                // write encrypted data to the file
                string cipherDataFilePath = dataFilePath.Insert(dataFilePath.LastIndexOf("."), "(SymEnc)");
                File.WriteAllBytes(cipherDataFilePath, encrypted);
            }
            catch (Exception e)
            {
                ExceptionHandler.OutputMessage(e.Message);
            }

        }

        // decrypt file content, write plainText to new file
        public void Decrypt(string dataFilePath, string parametersFilePath, string mode)
        {
            try
            {
                byte[] cipherText;
                // read data to decrypt
                cipherText = File.ReadAllBytes(dataFilePath);

                // read decryption parameters from file
                SymCryptParameters parameters = ReadParametersFromFile(parametersFilePath);
                CipherMode cipherMode = StringToCipherMode(mode);

                string decrypted = Decrypt(cipherText, parameters, cipherMode);

                // write decrypted data to the file
                string plainDataFilePath = dataFilePath.Insert(dataFilePath.LastIndexOf("."), "(SymDec)");
                File.WriteAllText(plainDataFilePath, decrypted);
            }
            catch (Exception e)
            {
                ExceptionHandler.OutputMessage(e.Message);
            }
        }

        // encrypt text to byte array
        private byte[] Encrypt(string plainText, SymCryptParameters parameters, CipherMode mode)
        {
            byte[] encrypted;

            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                aes.IV = parameters.IV;
                aes.Key = parameters.Key;
                aes.Mode = mode;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return encrypted;
        }


        // encrypt byte array to string
        private string Decrypt(byte[] cipherText, SymCryptParameters parameters, CipherMode mode)
        {
            string plaintext = null;

            // Create an AesCryptoServiceProvider object
            // with the specified key and IV.
            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                aes.Key = parameters.Key;
                aes.IV = parameters.IV;
                aes.Mode = mode;

                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }


        private SymCryptParameters ReadParametersFromFile(string filePath)
        {
            SymCryptParameters parameters;
            using (StreamReader reader = new StreamReader(filePath))
            {
                string json = reader.ReadToEnd();
                parameters = JsonConvert.DeserializeObject<SymCryptParameters>(json);
            }
            return parameters;
        }

        private CipherMode StringToCipherMode(string stringMode)
        {
            switch (stringMode.ToUpper())
            {
                case "ECB":
                    return CipherMode.ECB;
                case "CFB":
                    return CipherMode.CFB;
                case "CBC":
                    return CipherMode.CBC;
                default:
                    throw NewCryptoAlgorithmException();
            }
        }

        private CryptoAlrithmException NewCryptoAlgorithmException()
        {
            string description = "Invalid mode value. Allowed values are: ECB CFB CBC";
            return new CryptoAlrithmException(description);
        }

    }
}
