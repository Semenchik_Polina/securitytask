﻿using System;

namespace SecurityTask.CryptoAlgorithms
{
    public class CryptoAlrithmException : ArgumentException
    {
        public CryptoAlrithmException(string message) :
            base(message)
        { }
    }
}
