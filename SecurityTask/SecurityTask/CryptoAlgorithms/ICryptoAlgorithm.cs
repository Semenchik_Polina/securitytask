﻿namespace SecurityTask
{
    interface ICryptoAlgorithm
    {
        void Encrypt(string dataFilePath, string parametersFilePath, string mode);
        void Decrypt(string dataFilePath, string parametersFilePath, string mode);
    }
}
