﻿using SecurityTask.CryptoAlgorithms;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace SecurityTask
{
    public class AsymmetricCrypto : ICryptoAlgorithm
    {
        // encrypt file content, write cipherText to new file
        public void Encrypt(string dataFilePath, string parametersFilePath, string mode)
        {
            try
            {
                byte[] data = File.ReadAllBytes(dataFilePath);
                string parameters = File.ReadAllText(parametersFilePath);
                int keySize = GetKeySize(mode);
                byte[] encrypted;

                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(keySize))
                {
                    rsa.FromXmlString(parameters);
                    encrypted = rsa.Encrypt(data, false);
                }

                string encryptedFilePath = dataFilePath.Insert(dataFilePath.LastIndexOf("."), "(AsEnc)");
                File.WriteAllBytes(encryptedFilePath, encrypted);
            }
            catch (Exception e)
            {
                ExceptionHandler.OutputMessage(e.Message);
            }
        }

        // decrypt file content, write plainText to new file
        public void Decrypt(string dataFilePath, string parametersFilePath, string mode)
        {
            try
            {
                byte[] encryptedData = File.ReadAllBytes(dataFilePath);
                string parameters = File.ReadAllText(parametersFilePath);
                int keySize = GetKeySize(mode);
                byte[] decryptedData;


                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(keySize))
                {
                    rsa.FromXmlString(parameters);
                    decryptedData = rsa.Decrypt(encryptedData, false);
                }

                string plainData = Encoding.UTF8.GetString(decryptedData);
                string plainDataFilePath = dataFilePath.Insert(dataFilePath.LastIndexOf("."), "(AsDec)");
                File.WriteAllText(plainDataFilePath, plainData);
            }
            catch (Exception e)
            {
                ExceptionHandler.OutputMessage(e.Message);
            }
        }

        private int GetKeySize(string stringMode)
        {
            int mode = Int32.Parse(stringMode);
            if (mode == 1024 && mode == 2048)
            {
                return mode;
            }
            else throw NewCryptoAlgorithmException();
        }

        private CryptoAlrithmException NewCryptoAlgorithmException()
        {
            string description = "Invalid mode value. Allowed values are: 1024 2048";
            return new CryptoAlrithmException(description);
        }

    }
}
