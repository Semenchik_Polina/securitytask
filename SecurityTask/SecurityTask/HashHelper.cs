﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace SecurityTask
{
    // based on sha1
    public class HashHelper
    {
        
        // less then 20 chars (size of sha1 hash), can include non-printable characters 
        private const byte MaxPasswordLengthConst = 17;
        private const byte MinPasswordLengthConst = 7;

        // finds non-printable and whitespace characters
        private const string OutOfPasswordPattern = "[^#-~]+";

        // min password length for 
        private byte minPasswordLength = 10;

        public string StaticSalt { get; set; } = "xu3nDZ,J/3m3!D;||c>R>GX/P`+JM.";

        public byte MinPasswordLength
        {
            get { return minPasswordLength; }
            set
            {
                if (value < MinPasswordLengthConst)
                    minPasswordLength = MinPasswordLengthConst;
                else if (value > MaxPasswordLengthConst)
                    minPasswordLength = MaxPasswordLengthConst;
                else
                    minPasswordLength = value;
            }
        }
        
        // get hash of an object
        public string GetHash(Object obj)
        {
            string hash = "";

            try
            {
                if (obj == null)
                {
                    throw new NullReferenceException("Null as a paramater of GetHash function");
                }
                hash = BitConverter.ToString(GetHashByteArr(obj));
            }
            catch(Exception e)
            {
                ExceptionHandler.OutputMessage(e.Message);
            }
            return hash;
        }                   
        

        // get hash of user's password
        public string GetPasswordHash(UserProfile userProfile)
        {
            string hash = "";

            try
            {
                if (userProfile == null)
                {
                    throw new NullReferenceException("Null as a paramater of GetPasswordHash function");
                }
                byte[] dynamicSaltInBytes = GetDynamicSaltByteArr(userProfile);
                byte[] data = Encoding.UTF8.GetBytes(userProfile.Password);

                byte[] hashByteArr = GetHashByteArr(data);
                hashByteArr = GetHashByteArrWithSalt(hashByteArr, dynamicSaltInBytes);
                hashByteArr = GetHashByteArrWithSalt(hashByteArr, Encoding.UTF8.GetBytes(StaticSalt));

                hash = BitConverter.ToString(hashByteArr);
            }
            catch (Exception e)
            {
                ExceptionHandler.OutputMessage(e.Message);
            }

            return hash;
        }
        

        public string GetDynamicSalt(UserProfile userProfile)
        {
            string salt = "";

            try
            {
                if (userProfile == null)
                {
                    throw new NullReferenceException("Null as a paramater of GetHash function");
                }
                salt = BitConverter.ToString(GetDynamicSaltByteArr(userProfile));
            }
            catch (Exception e)
            {
                ExceptionHandler.OutputMessage(e.Message);
            }
            return salt;
        }

        // generate password for user's profile using hashing it's content
        public string GeneratePassword(UserProfile userProfile)
        {
            string password = "";

            try
            {
                if (userProfile == null)
                {
                    throw new NullReferenceException("Null as a paramater of GeneratePassword function");
                }
                using (SHA1 sha = new SHA1CryptoServiceProvider())
                {
                    Random random = new Random();
                    while (password.Length < MinPasswordLength)
                    {
                        string data = $"{userProfile.Id}.{userProfile.Email}.{random.Next()}";
                        byte[] hashedData = sha.ComputeHash(Encoding.UTF8.GetBytes(data));
                        string hashedDataStr = Encoding.UTF8.GetString(hashedData);
                        password = TrimNonPrintableChars(hashedDataStr);
                    }
                }
            }
            catch (Exception e)
            {
                ExceptionHandler.OutputMessage(e.Message);
            }           

            return password;
        }
        
        // delete all non-printable and whitespace characters
        private string TrimNonPrintableChars(string str)
        {
            Regex reg_exp = new Regex(OutOfPasswordPattern);
            return reg_exp.Replace(str, "");
        }

        // get hash of any object in bytes
        private byte[] GetHashByteArr(Object obj)
        {
            byte[] hashByteArr;

            using (SHA1 sha = new SHA1CryptoServiceProvider())
            {
                hashByteArr = sha.ComputeHash(ByteArrayHelper.ObjectToByte(obj));
            }
            return hashByteArr;
        }

        // generate dynamic salt for hashing user's data
        private byte[] GetDynamicSaltByteArr(UserProfile userProfile)
        {
            byte[] hashedSalt;
            string salt= $"{userProfile.PhoneNumber}.{userProfile.DateOfBirth}";

            using (SHA1 sha = new SHA1CryptoServiceProvider())
            {
                hashedSalt = sha.ComputeHash(Encoding.UTF8.GetBytes(salt));
            }                

            return hashedSalt;
        }

        // get hash with salt
        private byte[] GetHashByteArrWithSalt(byte[] data, byte[] salt)
        {
            byte[] hash;
            byte[] rawData = ByteArrayHelper.ConcatByteArrays(data, salt);

            using (SHA1 sha = new SHA1CryptoServiceProvider())
            {
                hash = sha.ComputeHash(rawData);
            }             

            return hash;
        }

    }
}
