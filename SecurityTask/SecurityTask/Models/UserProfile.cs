﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace SecurityTask
{
    [Serializable]
    public class UserProfile
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }

        // initialize with json file
        public UserProfile(string jsonFilePath)
        {
            UserProfile userProfile = null;
            try
            {
                using (StreamReader reader = new StreamReader(jsonFilePath))
                {
                    string json = reader.ReadToEnd();
                    userProfile = JsonConvert.DeserializeObject<UserProfile>(json);

                    Id = userProfile.Id;
                    Name = userProfile.Name;
                    Email = userProfile.Email;
                    Password = userProfile.Password;
                    PhoneNumber = userProfile.PhoneNumber;
                    DateOfBirth = userProfile.DateOfBirth;
                }
            }
            catch (Exception e)
            {
                ExceptionHandler.OutputMessage(e.Message);
            }       
        }

    }   

}
