﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityTask
{
    public class SymCryptParameters
    {
        public byte[] IV { get; set; }
        public byte[] Key { get; set; }

        public SymCryptParameters(byte[] iv, byte[] key)
        {
            IV = iv;
            Key = key;
        }
    }
}
