﻿using System;

namespace SecurityTask
{
    class Program
    {
        private const string PROFILEFOLDERPATH = "../../../data/userProfiles/";
        private const string PARAMETERFOLDERPATH = "../../../data/cryptParameters/";

        static void Main(string[] args)
        {
            Console.WriteLine("Security task");
            Console.WriteLine("Type help for a list of commands");

            HashHelper hashHelper = new HashHelper();
            SymmetricCrypto symmetricCrypto = new SymmetricCrypto();
            AsymmetricCrypto asymmetricCrypto = new AsymmetricCrypto();

            while (true)
            {
                string commandStr = Convert.ToString(Console.ReadLine());
                if (commandStr.Length < 1)
                {
                    continue;
                }
                string[] commandArr = commandStr.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                switch (commandArr[0])
                {
                    case "help":
                        DisplayCommandList();
                        break;

                    case "enc":
                        if (commandArr.Length == 5)
                        {
                            string encFilePath = String.Concat(PROFILEFOLDERPATH, commandArr[2]);
                            string encParametersFilePath = String.Concat(PARAMETERFOLDERPATH, commandArr[3]);
                            if (commandArr[1] == "-as")
                            {
                                asymmetricCrypto.Encrypt(encFilePath, encParametersFilePath, commandArr[4]);
                            }
                            else
                            {
                                symmetricCrypto.Encrypt(encFilePath, encParametersFilePath, commandArr[4]);
                            }
                        }
                        Console.WriteLine("encrypted successfully");
                        break;

                    case "dec":
                        if (commandArr.Length == 5)
                        {
                            string decFilePath = String.Concat(PROFILEFOLDERPATH, commandArr[2]);
                            string decParametersFilePath = String.Concat(PARAMETERFOLDERPATH, commandArr[3]);
                            if (commandArr[1] == "-as")
                            {
                                asymmetricCrypto.Decrypt(decFilePath, decParametersFilePath, commandArr[4]);
                            }
                            else
                            {
                                symmetricCrypto.Decrypt(decFilePath, decParametersFilePath, commandArr[4]);
                            }
                        }
                        Console.WriteLine("decrypted successfully");
                        break;

                    case "pass":
                        if (commandArr.Length == 2)
                        {
                            string password = hashHelper.GeneratePassword(new UserProfile(commandArr[1]));
                            Console.WriteLine("new password: {0}", password);
                        }
                        break;

                    case "hash":
                        if (commandArr.Length == 2)
                        {
                            string hash = hashHelper.GetHash(new UserProfile(commandArr[1]));
                            Console.WriteLine("hash: {0}", hash);
                        }
                        break;

                    case "dsalt":
                        if (commandArr.Length == 2)
                        {
                            string salt = hashHelper.GetDynamicSalt(new UserProfile(commandArr[1]));
                            Console.WriteLine("dynamic salt: {0}", salt);
                        }
                        break;

                    case "hpas":
                        if (commandArr.Length == 2)
                        {
                            string hash = hashHelper.GetPasswordHash(new UserProfile(commandArr[1]));
                            Console.WriteLine("hash of the password: {0}", hash);
                        }
                        break;

                    case "exit":
                        return;

                    default:
                        Console.WriteLine("'{0}' is not recognized as a command", commandStr);
                        break;
                }
            }

        }

        private static void DisplayCommandList()
        {
            Console.WriteLine("help     display list of commands");
            Console.WriteLine("enc ");
            Console.WriteLine("    -sym DATAFILENAME PARAMETERSFILENAME MODE    create symmetrical encrypted copy of the file");
            Console.WriteLine("    -as DATAFILENAME PARAMETERSFILENAME MODE    create asymmetrical encrypted copy of the file");
            Console.WriteLine("dec ");
            Console.WriteLine("    -sym DATAFILENAME PARAMETERSFILENAME MODE    create symmetrical decrypted copy of the file");
            Console.WriteLine("    -as DATAFILENAME PARAMETERSFILENAME MODE    create asymmetrical decrypted copy of the file");
            Console.WriteLine("hash FILENAME    display hash of the file");
            Console.WriteLine("pass FILENAME    generate password using the content of file");
            Console.WriteLine("dsalt FILENAME   display dynamic salt using the content of file");
            Console.WriteLine("hpas FILENAME    generate hash of password using the content of file");
            Console.WriteLine("exit    exit the application");
        }
    }
}
